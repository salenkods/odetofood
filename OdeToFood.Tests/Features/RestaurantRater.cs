﻿using System;
using System.Linq;
using OdeToFood.Models;

namespace OdeToFood.Tests.Features
{
	internal class RestaurantRater
	{
		private Restaurant _restaurant;

		public RestaurantRater(Restaurant restaurant)
		{
			_restaurant = restaurant;
		}

		public RatingResult ComputeResult(IRatingAlgorithm ratingAlgorithm, int numberOfReviewsToUse)
		{
			var filteredReviews = _restaurant.Reviews.Take(numberOfReviewsToUse);
			return ratingAlgorithm.Compute(filteredReviews.ToList());
		}
	}
}