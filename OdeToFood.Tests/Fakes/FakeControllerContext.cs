﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OdeToFood.Tests.Fakes
{
	public class FakeControllerContext : ControllerContext
	{
		HttpContextBase _context = new FakeHttpContext();

		public override HttpContextBase HttpContext
		{
			get
			{
				return _context;
			}
			set
			{
				_context = value;
			}
		}
	}

	public class FakeHttpContext : HttpContextBase
	{
		HttpRequestBase _request = new FakeHttpRequest();

		public override HttpRequestBase Request => _request;
	}

	public class FakeHttpRequest : HttpRequestBase
	{
		public override string this[string key] => null;

		public override NameValueCollection Headers => new NameValueCollection();
	}
}
