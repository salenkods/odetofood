﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OdeToFood.Models
{
	public class MaxWordsAttribute : ValidationAttribute
	{
		private readonly int _maxWords;

		public MaxWordsAttribute(int maxWords)
		{
			_maxWords = maxWords;
		}

		protected override ValidationResult IsValid(object value, ValidationContext validationContext)
		{
			if (value != null)
			{
				var valueAsString = value.ToString();
				if (valueAsString.Split(' ').Length > _maxWords)
				{
					var errorMessage = FormatErrorMessage(validationContext.DisplayName);
					return new ValidationResult(errorMessage);
				}
			}

			return ValidationResult.Success;
		}
	}

	public class RestaurantReview : IValidatableObject
	{
		public int Id { get; set; }

		[Range(1, 10)]
		[Required]
		public int Rating { get; set; }

		[Required]
		[StringLength(1024)]
		public string Body { get; set; }
		public int RestaurantId { get; set; }

		[Display(Name = "User Name")]
		[DisplayFormat(NullDisplayText = "anonymous")]
		[MaxWordsAttribute(1, ErrorMessage = "too big")]
		public string ReviewerName { get; set; }

		public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		{
			if (Rating < 2 && ReviewerName == "scott")
			{
				yield return new ValidationResult("Scott can't do this!");
			}
		}
	}
}